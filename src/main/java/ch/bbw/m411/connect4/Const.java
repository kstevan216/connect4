package ch.bbw.m411.connect4;

/**
 * All constant variables, which are needed for the programm
 */
public class Const {

    public static final int WIDTH = 7;

    public static final int HEIGHT = 4;

    public static final int NO_MOVE = -1;

    public static int BEST_MOVE = -1;

    public static int MAX_DEPTH = 4;

    public static int INITIAL_FREE_FIELDS;
}
