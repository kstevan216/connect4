package ch.bbw.m411.connect4;


/**
 * This stone enum represents the blue and red chips,
 * which are needed to play connect four
 */
public enum Stone {
    RED, BLUE;

    public Stone opponent() {
        return this == RED ? BLUE : RED;
    }
}
