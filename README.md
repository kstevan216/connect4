# Connect Four Template

A start of a project implementing a variant of the game "Connect Four".

## Mission

1. Extend the existing code such that the GreedyPlayer works.
2. Implement a strong Opponent using MinMax/Negamax with fixed depth (like 10).
3. Implement a perfect Player and optimize it for speed.

## Resources

- https://de.wikipedia.org/wiki/Minimax-Algorithmus#Implementierung
- https://de.wikipedia.org/wiki/Alpha-Beta-Suche#Implementierung
- https://en.wikipedia.org/wiki/Zobrist_hashing

## Fazit
Das Projekt konnte erfolgreich innerhalb des geplanten Zeitrahmens realisiert werden, da alle notwendigen Anforderungen erfüllt wurden.

Der Projektverlauf entwickelte sich so, dass kurz vor Projektende die Produktivität einen deutlichen Aufschwung verzeichnete. Dies war auf meine Entscheidung zurückzuführen, den Code in einzelne Klassen zu unterteilen, was es erleichterte, die verschiedenen Funktionen besser zu verstehen und entsprechend Anpassungen und neue Funktionen zu implementieren.

Optimierungen wie Alpha Beta oder Move Sort konnten aus Zeitgründen leider nicht umgesetzt werden.